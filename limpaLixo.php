<?php
	
	/*
		Arquivo de contenção.
		A ideia é limpar alguns lixos que são incluidos na hora de adicionar alguns campos
	*/
	
	include ("class/classe_pdo.php");
	
	
	$sql = " SELECT * FROM categoria_sub_arquivo where arquivo_status = '1' ";
	
	$res = $pdo -> query ($sql);
	
	while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
	{
		$row['arquivo_autores'] = preg_replace('/style="[a-z0-9-:&;A-Z _,.#]*"/','',$row['arquivo_autores']);
		$row['arquivo_autores'] = preg_replace('/<\/span[a-z0-9-:&;A-Z _,.]*>/','',$row['arquivo_autores']);
		$row['arquivo_autores'] = preg_replace('/<span[a-z0-9-:&;A-Z _,.]*>/','',$row['arquivo_autores']);
		$row['arquivo_autores'] = preg_replace('/<pre>/','',$row['arquivo_autores']);
		$row['arquivo_autores'] = preg_replace('/<\/pre>/','',$row['arquivo_autores']);
		
		$row['arquivo_abstract'] = preg_replace('/style="[a-z0-9-:&;A-Z _,.#]*"/','',$row['arquivo_abstract']);
		$row['arquivo_abstract'] = preg_replace('/<\/span[a-z0-9-:&;A-Z _,.]*>/','',$row['arquivo_abstract']);
		$row['arquivo_abstract'] = preg_replace('/<span[a-z0-9-:&;A-Z _,.]*>/','',$row['arquivo_abstract']);
		$row['arquivo_abstract'] = preg_replace('/<pre>/','',$row['arquivo_abstract']);
		$row['arquivo_abstract'] = preg_replace('/<\/pre>/','',$row['arquivo_abstract']);
		
		$row['arquivo_resumo'] = preg_replace('/style="[a-z0-9-:&;A-Z _,.#]*"/','',$row['arquivo_resumo']);
		$row['arquivo_resumo'] = preg_replace('/<\/span[a-z0-9-:&;A-Z _,.]*>/','',$row['arquivo_resumo']);
		$row['arquivo_resumo'] = preg_replace('/<pre>/','',$row['arquivo_resumo']);
		$row['arquivo_resumo'] = preg_replace('/<\/pre>/','',$row['arquivo_resumo']);
		
		
		$sql = 
		" 
			UPDATE 
				categoria_sub_arquivo
			SET
				arquivo_autores = '{$row['arquivo_autores']}',
				arquivo_abstract = '{$row['arquivo_abstract']}',
				arquivo_resumo = '{$row['arquivo_resumo']}'
			WHERE
				id_categoria_sub_arquivo = '{$row['id_categoria_sub_arquivo']}'
		";
		
		$query = $pdo -> query ($sql);
		echo "<p> {$row['id_categoria_sub_arquivo']} alterado. </p>";
		
	}
	


?>