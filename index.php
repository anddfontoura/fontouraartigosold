<?php


	session_start();
	
	include_once ("func.php");
	include_once ("class/classe_pdo.php");
	include_once ("class/classe_login.php");
	include_once ("class/classe_categoria.php");
	include_once ("class/classe_categoria_sub.php");
	include_once ("class/classe_arquivo.php");
	include_once ("class/classe_pagina.php");
		
	if ( isset ($_GET['mod']) ) { $mod = limpaDados($_GET['mod']); } else { $mod = "Home"; } 
	if ( $mod == "Sair" ) { unset ($_SESSION['usuario_logado']); } 
?>


<!DOCTYPE html>
	<html lang='pt-br'>
	<head>
		<meta charset='UTF8'></meta>
		<title>Biblioteca de Artigos - Fontoura Editora</title>
	
			<!-- Latest compiled and minified CSS -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<link rel="stylesheet" href="css/estilo.css">
			<link rel="stylesheet" href="css/estilo_submenu.css">
			<!-- jQuery library -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
			<!-- Latest compiled JavaScript -->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<script src="js/fontoura.js" type="text/javascript"></script>
			<script src="3rd/ckeditor/ckeditor.js"></script>
	</head>
	
	
	<body>
		<div id="pagewrap">

			<header>
				<img src='img/logo.png'>
				
				<?php $classe_pagina -> ListaPaginaHome($pdo); ?>
			</header>
				
			<section id="content">
				<div class='width-100 login'>
					<form action='?mod=Login&opt=Logar-Sistema' method='POST'>
						<input type='text' name='sistema_usuario'> </input>
						<input type='password' name='sistema_senha'> </input>
						<input type='submit' value='Logar no sistema'> </input>
					</form>
				</div>
				
				<div class='width-100 pesquisar'>
					<h3> Pesquisar </h3>
					<form action='?mod=Arquivo&opt=Pesquisar' method='POST'>
						<input type='text' name='pesquisar_sistema' placeholder='Digite a Palavra-Chave ou termo'> </input>
						<input type='submit' value='Pesquisar nas coleções'> </input>
					</form>
				</div>
				
					<h3> Publicações </h3>
				<?php $classe_categoria -> ListaCategoriaHome ($pdo); ?>
			</section>
			
				
			<section id='middle'>
				
				
			
				<?php
					
					if ( isset($_SESSION['usuario_logado']) && ($_SESSION['usuario_logado'] == VALIDO) ) { $classe_login -> menuSistema(); }
					
					switch ( $mod ) 
					{
										
						case "Login":					include_once("modules/login/index.php"); break;
						case "Categoria":				include_once("modules/categoria/index.php"); break;
						case "CategoriaSub":			include_once("modules/categoria_sub/index.php"); break;
						
						case "Arquivo":					include_once("modules/arquivo/index.php"); break;
						case "Home":					
						case "Pagina":					include_once("modules/pagina/index.php"); break;
						
						
						default:	echo "<p> Essa página está em desenvolvimento </p>";
					}
				
				?>
			</section>

			<aside id="sidebar">
				<?php $classe_arquivo -> UltimosArquivosHome($pdo); ?>
			</aside>
			
			
			<footer>
				<a href='http://www.anddfontoura.com.br' target='_BLANK'>
					<h4>AnddFontoura</h4>
					<h5>Desenvolvimento WEB</h5>
				</a>
			</footer>
	
		</div>

	</body>
	
</html>