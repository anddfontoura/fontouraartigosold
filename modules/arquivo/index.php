<?php

	/*
		arquivo/index.php
	*/
	
	if ( isset ($_GET['opt']) ) { $opt = $_GET['opt']; } else { $opt = "Home"; }
	
	switch ( $opt )
	{
		case "Home":				include_once ("view/vi_home.php");break;
		
		case "Edit-Arquivo":		
		case "Form-Arquivo":		if ( $_SESSION['usuario_logado'] == VALIDO ) { include_once("model/mo_form_arquivo.php"); } break;
		case "Cat-Arquivo":			if ( $_SESSION['usuario_logado'] == VALIDO ) { include_once("catalog/cat_arquivo.php"); } break;
		case "List-Arquivo":		if ( $_SESSION['usuario_logado'] == VALIDO ) { include_once("view/vi_lista_arquivo.php"); } break;
		
		case "Lista-Arquivos-Home":		include_once("view/vi_lista_arquivo_home.php"); break;
		
		case "Arquivo-Detalhe":		include_once("view/vi_lista_arquivo_detalhes.php"); break;
		
		case "Pesquisar":			include_once ("view/vi_pesquisa.php"); break;
		
		default:	echo "<div class='div-fail'> <p> Algo errado não está certo </p> </div> ";
	}
	
?>