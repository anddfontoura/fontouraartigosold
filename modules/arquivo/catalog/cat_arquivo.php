<?php

	/*
		catalog/cat_arquivo.php
	*/
		
	if ( isset ($_POST) && !empty($_POST) )
	{
		$arquivo_titulo = limpaDados($_POST['arquivo_titulo']);
		$arquivo = $_FILES['arquivo'];
		$id_categoria_sub = removeLetra($_POST['id_categoria_sub']);
		$arquivo_abstract = $_POST['arquivo_abstract'];
		$arquivo_autor = $_POST['arquivo_autor'];
		$arquivo_resumo = $_POST['arquivo_resumo'];
		if ( isset ($_GET['id_arquivo']) ) { $id_arquivo = removeLetra($_GET['id_arquivo']); } else { $id_arquivo = FALSE; } 
		//print_r($obra_arquivo);
		
		if ( $id_arquivo !== FALSE )
			$classe_arquivo -> updateArquivo ( $pdo, $id_arquivo, $arquivo_titulo, $arquivo, $id_categoria_sub, $arquivo_abstract, $arquivo_resumo, $arquivo_autor);
		else
			$classe_arquivo -> incluirArquivo( $pdo, $arquivo_titulo, $arquivo, $id_categoria_sub, $arquivo_abstract, $arquivo_resumo, $arquivo_autor );
			
	}
		
?>