<?php
	
	/*
		model/mo_form_arquivo.php
	*/
	
	if ( isset ($_GET['id_arquivo']) )
	{
		$id_arquivo = removeLetra($_GET['id_arquivo']);
		
		$dados_arquivo = $classe_arquivo -> returnArquivo( $pdo, 1, $id_arquivo, FALSE, FALSE, FALSE );
		
		//var_dump($dados_arquivo);
		
		$id_categoria_sub = $dados_arquivo[1]['categoria_sub_id'];
	} else {
		
	}
	
?>
	<form action='?mod=Arquivo&opt=Cat-Arquivo<?php if (!empty($dados_arquivo) ) { echo "&id_arquivo=".$dados_arquivo[1]['id_categoria_sub_arquivo']; } ?>' enctype='multipart/form-data' method='POST'>
		<h3> Titulo do Arquivo </h3>
		<input type='text' name='arquivo_titulo' value='<?php if (!empty($dados_arquivo) ) { echo $dados_arquivo[1]['arquivo_nome']; } ?>'> </input>
		
		<h3> Arquivo </h3>
		<input type='FILE' name='arquivo'> </input>
		
		<h3> Categoria e Sub Categoria </h3>
		<?php $classe_categoria_sub -> selectCategoriaSub ( $pdo, FALSE ); ?>
		
		<h3> Autor(es) </h3>
		<textarea name='arquivo_autor' class='editor1' id='editor1'><?php if (!empty($dados_arquivo) ) { echo $dados_arquivo[1]['arquivo_autores']; } ?></textarea>	
		
		<h3> Resumo </h3>
		<textarea name='arquivo_resumo' class='editor2' id='editor2'><?php if (!empty($dados_arquivo) ) { echo $dados_arquivo[1]['arquivo_resumo']; } ?></textarea>
		
		<h3> Abstract </h3>
		<textarea name='arquivo_abstract' class='editor3' id='editor3'><?php if (!empty($dados_arquivo) ) { echo $dados_arquivo[1]['arquivo_abstract']; } ?></textarea>
		
		<input type='submit' value='Incluir Arquivo'> </input>
	
	</form>
	
	<script>
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
		CKEDITOR.replace( 'editor1' );
		CKEDITOR.replace( 'editor2' );
		CKEDITOR.replace( 'editor3' );
	</script>