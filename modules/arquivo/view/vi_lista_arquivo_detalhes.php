<?php
	
	/*
		view/vi_lista_arquivo_detalhes.php
	*/

	if ( isset($_GET['limit']) )
	{
		$limit = removeLetra($_GET['limit']);
	} else {
		$limit = 1;
	}
	
	if ( isset($_GET['id_arquivo']) )
	{
		$id_arquivo = removeLetra($_GET['id_arquivo']);
	} else {
		$id_arquivo = 1;
	}
	
	$dados_arquivo = $classe_arquivo -> returnArquivo ($pdo, $limit, $id_arquivo, FALSE, FALSE);
	
	if ( !empty($dados_arquivo) )
	{
		//var_dump($dados_arquivo[0]);
		
		for ( $i = 1; $i < sizeof($dados_arquivo); $i++ )
		{
			
			echo 
			"
				<h2>{$dados_arquivo[$i]['arquivo_nome']}</h2>
								
				<a href='{$dados_arquivo[$i]['arquivo_caminho']}' target='blank'>
					<div class='alert alert-success'>
						<span class='glyphicons glyphicons-download-alt'></span>
						Para baixar o artigo clique aqui
					</div>		
				</a>
				
				<div class='panel panel-default'>
					<div class='panel-heading'> 
						Autor(es)
					</div>
					
					<div class='panel-body justify'>
						{$dados_arquivo[$i]['arquivo_autores']}
					</div>
				</div>
				
				<div class='panel panel-default'>
					<div class='panel-heading'> 
						Resumo
					</div>
					
					<div class='panel-body justify'>
						{$dados_arquivo[$i]['arquivo_resumo']}
					</div>
				</div>
				

			";
			
			if ( $dados_arquivo[$i]['arquivo_abstract'] != "" )
			{
				echo "
					<div class='panel panel-default'>
						<div class='panel-heading'> 
							Abstract
						</div>
						
						<div class='panel-body justify'>
							{$dados_arquivo[$i]['arquivo_abstract']}
						</div>
					</div>
				";
			}
			
		}
		
		//criaPaginacao ($pdo, $limit, $dados_arquivo[0], "?mod=Arquivo&opt=Lista-Arquivos-Home&id_categoria_sub=$id_categoria_sub&limit=");
	} else {
		echo "<div class='div-fail'> <p> Não há arquivos a serem listados </p> </div>";
	}
?>