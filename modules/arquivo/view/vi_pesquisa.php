<?php

	/*
		view/vi_pesquisa.php
	*/
	
	if ( isset ($_POST) && !empty($_POST) )
	{
		$pesquisar = limpaDados($_POST['pesquisar_sistema']);
		
		if ( strLen($pesquisar) < 5 )
		{
			echo "<p> Pedimos para que seja digitado mais do que 5 caracteres na pesquisa. </p>";
		} else {	
			echo 
			"	
				<div class='alert alert-success'>
					A pesquisa acontece no título, autores, abstract e resumo. Para ver detalhadamente a página do artigo relacionado basta clicar no quadro
					correspondente
				</div>	
				
				<p> Pesquisado: <b> {$pesquisar} </b> </p>
			";
			$dados_arquivo = $classe_arquivo -> returnPesquisaArquivo ( $pdo, $pesquisar );
		}
	}
?>