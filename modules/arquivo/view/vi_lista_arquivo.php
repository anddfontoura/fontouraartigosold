<?php
	
	/*
		view/vi_lista_arquivo.php
	*/

	if ( isset($_GET['limit']) )
	{
		$limit = removeLetra($_GET['limit']);
	} else {
		$limit = 1;
	}
	
	$dados_arquivo = $classe_arquivo -> returnArquivo ($pdo, $limit, FALSE, FALSE, FALSE);
	
	if ( !empty($dados_arquivo) )
	{
		
		for ( $i = 1; $i < sizeof($dados_arquivo); $i++ )
		{
			//var_dump ($dados_arquivo[$i]);
			
			echo 
			"
				<div class='width-100'> 
					<div class='div-opcoes'>
						<a href='?mod=Arquivo&opt=Edit-Arquivo&id_arquivo={$dados_arquivo[$i]['id_categoria_sub_arquivo']}' alt='Editar' title='Editar'> <img src='img/icon/edit.png'> </img> </a>
						<a href='?mod=Arquivo&opt=Delete-Arquivo&id_arquivo={$dados_arquivo[$i]['id_categoria_sub_arquivo']}' alt='Excluir' title='Excluir'> <img src='img/icon/delete.png'> </img> </a>
					</div>
					<p> 
						<p> <b>Arquivo:</b> {$dados_arquivo[$i]['arquivo_nome']}</b><br>
							<b>Categoria: </b>{$dados_arquivo[$i]['categoria_nome']}<br>
							<b>Sub Categoria: </b> {$dados_arquivo[$i]['categoria_sub_nome']}
						</p>
					</p>
				</div>
				<hr>
			";
		}
		
		criaPaginacao ($pdo, $limit, $dados_arquivo[0], "?mod=Arquivo&opt=List-Arquivo&limit=");
	} else {
		echo "<div class='div-fail'> <p> Não há arquivos a serem listados </p> </div>";
	}
?>