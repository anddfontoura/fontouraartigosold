<?php
	
	/*
		view/vi_lista_arquivo_home.php
	*/

	if ( isset($_GET['limit']) )
	{
		$limit = removeLetra($_GET['limit']);
	} else {
		$limit = 1;
	}
	
	if ( isset($_GET['id_categoria_sub']) )
	{
		$id_categoria_sub = removeLetra($_GET['id_categoria_sub']);
	} else {
		$id_categoria_sub = 1;
	}
	
	$dados_arquivo = $classe_arquivo -> returnArquivo ($pdo, $limit, FALSE, $id_categoria_sub, FALSE);
	
	if ( !empty($dados_arquivo) )
	{
		//var_dump($dados_arquivo[0]);
		
		for ( $i = 1; $i < sizeof($dados_arquivo); $i++ )
		{
			echo 
			"
				<a href='?mod=Arquivo&opt=Arquivo-Detalhe&id_arquivo={$dados_arquivo[$i]['id_categoria_sub_arquivo']}'> 
				<div class='panel panel-default'>
					<div class='panel-heading'>{$dados_arquivo[$i]['arquivo_nome']}</div>

						<p>{$dados_arquivo[$i]['arquivo_autores']}</p>		
					</div>
				</a>";
		}
		
		criaPaginacao ($pdo, $limit, $dados_arquivo[0], "?mod=Arquivo&opt=Lista-Arquivos-Home&id_categoria_sub=$id_categoria_sub&limit=");
	} else {
		echo "<div class='div-fail'> <p> Não há arquivos a serem listados </p> </div>";
	}
?>