<?php

	/*
		categoria/index.php
	*/
	
	
	if ( isset ($_GET['opt']) ) { $opt = $_GET['opt']; } else { $opt = "Home"; }
	
	switch ( $opt )
	{
		case "Home":				include_once("view/vi_lista_home.php");

		case "Edit-Categoria":		
		case "Form-Categoria":		if ( $_SESSION['usuario_logado'] == VALIDO ) { include_once("model/mo_form_categoria.php"); } break;
		case "Cat-Categoria":		if ( $_SESSION['usuario_logado'] == VALIDO ) { include_once("catalog/cat_categoria.php"); } break;
		case "List-Categoria":		if ( $_SESSION['usuario_logado'] == VALIDO ) { include_once("view/vi_lista_categoria.php"); } break;
		
		default:	echo "<div class='div-fail'> <p> Algo errado não está certo </p> </div> ";
	}
	
?>