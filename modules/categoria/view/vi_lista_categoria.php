<?php

	/*
		view/vi_lista_categoria.php
	*/
	
	if ( isset ($_GET['limit']) )
	{
		$limit = removeLetra($_GET['limit']);
	} else {
		$limit = 1;
	}
	
	$dados_categoria = $classe_categoria -> returnCategoria ( $pdo, $limit, FALSE, FALSE, TRUE );
	
	if ( !empty($dados_categoria) )
	{

		for ( $i = 1; $i < sizeof($dados_categoria); $i++ )
		{
			echo 
			"
				<div class='width-100'>
					<div class='div-opcoes'>
						<a href='?mod=Categoria&opt=Edit-Categoria&id_categoria={$dados_categoria[$i]['id_categoria']}' alt='Editar' title='Editar'> <img src='img/icon/edit.png'> </img> </a>
						<a href='?mod=Categoria&opt=Delete-Categoria&id_categoria={$dados_categoria[$i]['id_categoria']}' alt='Excluir' title='Excluir'> <img src='img/icon/delete.png'> </img> </a>
					</div>
					<p> <b> Categoria: </b> {$dados_categoria[$i]['categoria_nome']} </p>
				</div>
				<hr>
			";
		}
		
		criaPaginacao ($pdo, $limit, $dados_categoria[0], "?mod=Categoria&opt=List-Categoria&limit=");
	
	} else {
		echo "<div class='div-fail'> <p> Não há categorias cadastradas </p> </div>";
	}
	
	
?>