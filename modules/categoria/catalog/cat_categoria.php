<?php

	/*
		catalog/cat_categoria.php 
	*/
	
	if ( isset ($_POST) && !empty($_POST) )
	{
		$categoria_nome = limpaDados($_POST['categoria']);
		
		if ( isset ($_GET['id_categoria']) )
		{
			$id_categoria = removeLetra($_GET['id_categoria']);
			$classe_categoria -> updateCategoria ( $pdo, $id_categoria, $categoria_nome );
		} else
			$classe_categoria -> incluirCategoria( $pdo, $categoria_nome );
		
	}
?>