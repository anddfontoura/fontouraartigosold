<?php

	/*
		view/vi_lista_categoria_sub.php
	*/
	
	if ( isset ($_GET['limit']) )
	{
		$limit = removeLetra($_GET['limit']);
	} else {
		$limit = 1;
	}
	
	if ( isset ($_GET['id_categoria']) )
	{
		$id_categoria = removeLetra($_GET['id_categoria']);
	} else {
		$id_categoria = FALSE;
	}
	
	$dados_categoria_sub = $classe_categoria_sub -> returnCategoriaSub ( $pdo, $limit, FALSE, $id_categoria, FALSE, TRUE );
	
	if ( !empty ($dados_categoria_sub) )
	{
		for ( $i = 1; $i < sizeof($dados_categoria_sub); $i++ )
		{
			$dados_qtde_artigos = $classe_categoria_sub -> returnQuantidadeArquivosCategoria ( $pdo, $dados_categoria_sub[$i]['id_categoria_sub'] );
			//var_dump ($dados_qtde_artigos);
			
			echo 
			"
				<a href='?mod=Arquivo&opt=Lista-Arquivos-Home&id_categoria_sub={$dados_categoria_sub[$i]['id_categoria_sub']}'>
					<div class='panel panel-default'>
						<div class='panel-heading'>{$dados_categoria_sub[$i]['categoria_sub_nome']} </div>
						<div class='panel-body'>
							{$dados_qtde_artigos[1]['qtde_artigos']} Artigos 
						</div>
					</div>
				</a>
				
			";
		}
		
	} else {
		echo "<div> <p> Não há Sub Categorias cadastradas </p> </div>";
	}