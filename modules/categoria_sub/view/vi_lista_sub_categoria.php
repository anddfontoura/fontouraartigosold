<?php

	/*
		view/vi_lista_categoria_sub.php
	*/
	
	if ( isset ($_GET['limit']) )
	{
		$limit = removeLetra($_GET['limit']);
	} else {
		$limit = 1;
	}
	
	$dados_categoria_sub = $classe_categoria_sub -> returnCategoriaSub ( $pdo, $limit, FALSE, FALSE, FALSE, TRUE );
	
	if ( !empty ($dados_categoria_sub) )
	{
		for ( $i = 1; $i < sizeof($dados_categoria_sub); $i++ )
		{
			echo 
			"
				<div class='width-100'>
					<div class='div-opcoes'>
						<a href='?mod=CategoriaSub&opt=Edit-Sub-Categoria&id_categoria_sub={$dados_categoria_sub[$i]['id_categoria_sub']}' alt='Editar' title='Editar'> <img src='img/icon/edit.png'> </img> </a>
						<a href='?mod=CategoriaSub&opt=Delete-Sub-Categoria&id_categoria_sub={$dados_categoria_sub[$i]['id_categoria_sub']}' alt='Excuir' title='Excluir'> <img src='img/icon/delete.png'> </img> </a>
					</div>
					<p> <b> Categoria: </b> {$dados_categoria_sub[$i]['categoria_nome']} </p>
					<p> <b> Sub Categoria: </b> {$dados_categoria_sub[$i]['categoria_sub_nome']} </p>
				</div>
				<hr>
			";
		}
		
		criaPaginacao ($pdo, $limit, $dados_categoria_sub[0], "?mod=CategoriaSub&opt=List-Sub-Categoria&limit=");
		
	} else {
		echo "<div> <p> Não há Sub Categorias cadastradas </p> </div>";
	}