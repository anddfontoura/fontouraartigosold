<?php

	/*
		catalog/cat_categoria_sub.php
	*/
		
	if ( isset ($_POST) && !empty($_POST) )
	{
		$categoria_sub_nome = $_POST['categoria_sub'];
		$categoria_id = $_POST['id_categoria'];
		//$volume_editorial = $_POST['volume_editorial'];
		if ( isset ($_GET['id_categoria_sub']) ) { $id_categoria_sub = removeLetra($_GET['id_categoria_sub']); } else { $id_categoria_sub = FALSE; }
		
		if ( $id_categoria_sub !== FALSE )
			$classe_categoria_sub -> updateCategoriaSub ( $pdo, $id_categoria_sub, $categoria_id, $categoria_sub_nome );
		else
			$classe_categoria_sub -> incluirCategoriaSub ($pdo, $categoria_id, $categoria_sub_nome );
	}
		
?>