<?php

	/*
		view/vi_lista_categoria.php
	*/
	
	if ( isset ($_GET['limit']) )
	{
		$limit = removeLetra($_GET['limit']);
	} else {
		$limit = 1;
	}
	
	$dados_pagina = $classe_pagina -> returnPagina ( $pdo, $limit, FALSE, FALSE );
	
	if ( !empty($dados_pagina) )
	{

		for ( $i = 1; $i < sizeof($dados_pagina); $i++ )
		{
			echo 
			"
				<div class='width-100'>
					<div class='div-opcoes'>
						<a href='?mod=Pagina&opt=Edit-Pagina&id_pagina={$dados_pagina[$i]['id_pagina']}' alt='Editar' title='Editar' > <img src='img/icon/edit.png'> </img> </a>
						<a href='?mod=Pagina&opt=Delete-Pagina&id_pagina={$dados_pagina[$i]['id_pagina']}' alt='Excluir' title='Excluir' > <img src='img/icon/delete.png'> </img> </a>
					</div>
					<p> <B> Pagina: </b> {$dados_pagina[$i]['pagina_nome']} </p>
				</div>
				<hr>
			";
		}
		
		criaPaginacao ($pdo, $limit, $dados_pagina[0], "?mod=Pagina&opt=List-Pagina&limit=");
	
	} else {
		echo "<div class='div-fail'> <p> Não há páginas cadastradas </p> </div>";
	}
	
	
?>