<?php

	/*
		catalog/cat_pagina.php
	*/
	
	
	if ( isset ($_POST) && !empty($_POST) )
	{
		
		if ( isset ($_GET['id_pagina']) ) { $id_pagina = removeLetra($_GET['id_pagina']); } else { $id_pagina = FALSE; } 
		$pagina_titulo = $_POST['pagina_nome'];
		$pagina_conteudo = $_POST['pagina_conteudo'];
		
		if ( $id_pagina === FALSE )
			$classe_pagina -> incluirPagina ( $pdo, $pagina_titulo, $pagina_conteudo );
		else
			$classe_pagina -> updatePagina ( $pdo, $id_pagina, $pagina_titulo, $pagina_conteudo );
	} 
	
?>