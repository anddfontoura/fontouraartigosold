/* JS especiais */

/* Função para verificar se todos os campos foram devidamente preenchidos e são válidos */
function validacaoRegistro()
{
	
	var login 		= document.getElementById('log_1').value; 
	var senha1 		= document.getElementById('pass_1').value; 
	var senha2 		= document.getElementById('pass_2').value;
	var email 		= document.getElementById('mail_1').value;
	var nascimento 	= document.getElementById('date_1').value;
	
	
	
	/* Primeira validação, checa se algum campo não foi preenchido. */
	if ( login == "" && senha1 == "" && senha2 == "" && email == "" && nascimento == "" )
	{
		alert ("Você não preencheu algum campo, confira o formulário novamente.");
		return false;
	}
	
	/* Confirma se todos os campos são legítimos */
	var re = /[a-zA-Z0-9]/;
	//var re = new RegExp("ab+c");
	if ( re.test(login) != true )
	{
		alert ("O seu login contém um carácter não autorizado. Não utilize nada além de letra sem acentos e números ");
		return false;
	}
	
	var re = /[A-Za-z0-9._-]{1,50}@[A-Za-z0-9]{1,50}.[A-Za-z0-9]{1,3}.?[A-Za-z0-9]{1,3}/;
	if ( re.test(email) != true)
	{
		alert ("Seu e-mail não parece válido, troque-o e tente novamente.");
		return false;
	}
	
	var re = /[0-9]{2}\/[0-9]{2}\/[0-9]{2}/;
	if ( re.test(nascimento) != true )
	{
		alert ("Sua data de nascimento não está correta, favor escrever como no exemplo: dd/mm/yy");
		return false;
	}
	
	/* Confirma se as duas senhas são iguais */
	if ( senha1 != senha2 )
	{
		alert ("Suas senhas não são as mesmas, verifique se não houve um erro de digitação.");
		return false;
	}
	
	return true;
	
	
	
}

// autocomplet : this function will be executed every time we change the text
function complete_guild() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#id_guild').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'guild_search.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#guild_list_id').show();
				$('#guild_list_id').html(data);
			}
		});
	} else {
		$('#guild_list_id').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#id_guild').val(item);
	// hide proposition list
	$('#guild_list_id').hide();
}