<?php

	/*
		classe arquivo
	*/
	
	class Pagina
	{
		
		public function sqlPagina ( $id_pagina, $pagina_nome )
		{
			$sql =
			"
				SELECT 
					p.*
				FROM
					paginas p 
				WHERE
					1 = 1
					
			";
			
			if ( $id_pagina !== FALSE )
				$sql .= " AND p.id_pagina = '$id_pagina' ";
			
			if ( $pagina_nome !== FALSE )
				$sql .= " AND p.pagina_nome like '%$pagina_nome%' ";
			
			//$sql .= " ORDER BY p.pagina_nome ASC ";
			$sql .= " ORDER BY p.id_pagina ASC ";
			
			return $sql;
		}
		
		public function returnPagina ( $pdo, $limit, $id_pagina, $pagina_nome  )
		{
			$sql = $this -> sqlPagina ( $id_pagina, $pagina_nome  );
			$sql_s_l = $sql;
			$sql .= calculaLimit($limit);
			$res = $pdo -> query ($sql);
			
			//echo $sql;
			
			$dados_Categoria = array();
			
			if ( $res -> rowCount() > 0 )
			{
				array_push ($dados_Categoria, $sql_s_l );
				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					array_push($dados_Categoria, $row);
				}
			}
			
			return $dados_Categoria;
		}
		
		
		public function ListaPaginaHome($pdo)
		{
			$sql =  $this -> sqlPagina ( FALSE, FALSE );
			$res = $pdo -> query ($sql);
			
			if ( $res -> rowCount() > 0 )
			{
				echo "<div class='div-menu-paginas'>";
				
				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					echo " <a href='?mod=Home&id_pagina={$row['id_pagina']}'> <div > {$row['pagina_nome']} </div> </a> ";
				}
				
				echo "</div>";
			}
			
			
		}
		
		public function updatePagina ( $pdo, $id_pagina, $pagina_nome, $pagina_conteudo )
		{
			$sql =
			"
				UPDATE 
					paginas
				SET
					`pagina_nome` = '$pagina_nome',
					`pagina_conteudo` = '$pagina_conteudo' 
				WHERE
					`id_pagina` = '$id_pagina'
			";
			
			//echo $sql;
			
			$res = $pdo -> query ($sql);
			
			if ( $res != null)
				echo "<div class='div-sucess'> <p> A sub categoria foi atualizada com sucesso, já disponível para visualização </p> </div>";
			else
				echo "<div class='div-fail'> <p> Houve um erro ao atualizar essa sub categoria. Envie a linha a seguir ao desenvolvedor: <hr> $sql </p> </div>";
			
		}
		
		public function incluirPagina ($pdo, $pagina_nome, $pagina_conteudo )
		{
			/*
				Confere primeiro se o arquivo é válido
			*/
			//var_dump ($obra_arquivo);
			
			$sql = 
			"	
				INSERT INTO
					paginas
					(`pagina_nome`,`pagina_conteudo`)
					VALUES
					('$pagina_nome','$pagina_conteudo')
			";
			
			$res = $pdo -> query ($sql);
			
			if ( $res != null)
				echo "<div class='div-sucess'> <p> Artigo adicionado com sucesso, já disponível para visualização </p> </div>";
			else
				echo "<div class='div-fail'> <p> Houve um erro ao criar esse Artigo. Envie a linha a seguir ao desenvolvedor: <hr> $sql </p> </div>";
	
		}
		
	
	}
	
	$classe_pagina = new Pagina();

?>