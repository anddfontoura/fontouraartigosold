<?php

	/*
		class/classe_login.php
	*/
	
	class Login
	{
		
		public function menuSistema ()
		{
			echo
			"
				<div id='cssmenu'>
					<ul>					   

						<li class='has-sub'><a href='#'><span> Páginas </span></a>
							<ul>
								<li class='has-sub'><a href='?mod=Pagina&opt=Form-Pagina'><span>Incluir Página</span></a></li>
								<li class='has-sub'><a href='?mod=Pagina&opt=List-Pagina'><span>Listar Páginas </span></a></li>
							</ul>
						</li>
						
						<li class='has-sub'><a href='#'><span> Categoria </span></a>
							<ul>
								<li class='has-sub'><a href='?mod=Categoria&opt=Form-Categoria'><span> Incluir Categoria </span></a></li>
								<li class='has-sub'><a href='?mod=Categoria&opt=List-Categoria'><span> Listar Categoria </span></a></li>
							</ul>
						</li>
						
						<li class='has-sub'><a href='#'><span> Sub Categoria </span></a>
							<ul>
								<li class='has-sub'><a href='?mod=CategoriaSub&opt=Form-Sub-Categoria'><span> Incluir Sub Categoria </span></a></li>
								<li class='has-sub'><a href='?mod=CategoriaSub&opt=List-Sub-Categoria'><span> Listar Sub Categoria </span></a></li>
							</ul>
						</li>
						
						<li class='has-sub'><a href='#'><span> Arquivo </span></a>
							<ul>
								<li class='has-sub'><a href='?mod=Arquivo&opt=Form-Arquivo'><span> Incluir Arquivo </span></a></li>
								<li class='has-sub'><a href='?mod=Arquivo&opt=List-Arquivo'><span> Listar Arquivo </span></a></li>
							</ul>
						</li>
						
						<li><a href='?mod=Sair'><span> Sair </span></a></li>
					</ul>
				</div>
			";
		}
		
	}
	
	$classe_login = new Login();
?>