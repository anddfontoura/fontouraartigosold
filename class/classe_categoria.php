<?php

	/*
		classe categoria
	*/
	
	class Categoria
	{
		
		public function sqlListaCategoria ( $id_categoria, $categoria, $categoria_ativo )
		{
			$sql =
			"
				SELECT 
					*
				FROM 
					categoria
				WHERE
					1 = 1
			";
			
			if ( $id_categoria !== FALSE )
				$sql .= " AND id_categoria = '$id_categoria' ";
			
			if ( $categoria !== FALSE )
				$sql .= " AND categoria_nome like '%$categoria%' ";
			
			if ( $categoria_ativo === TRUE )
				$sql .= " AND categoria_status = '1' ";
			
			$sql .= " ORDER BY categoria_nome DESC ";
			
			return $sql;
		}
		
		public function returnCategoria ( $pdo, $limit, $id_categoria, $categoria_nome, $categoria_status )
		{
			$sql = $this -> sqlListaCategoria ( $id_categoria, $categoria_nome, $categoria_status );
			$sql_s_l = $sql;
			$sql .= calculaLimit($limit);
			$res = $pdo -> query ($sql);
			
			
			$dados_Categoria = array();
			
			if ( $res -> rowCount() > 0 )
			{
				array_push ($dados_Categoria, $sql_s_l );
				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					array_push($dados_Categoria, $row);
				}
			}
			
			return $dados_Categoria;
		}
			
		public function selectCategoria ($pdo, $id_categoria)
		{
			$sql = $this -> sqlListaCategoria ( false, FALSE, TRUE );
			$res = $pdo -> query ($sql);
			
			echo "<select name='id_categoria' > ";
			
			if ( $res -> rowCount() > 0 )
			{
				while  ($row = $res -> fetch ( PDO::FETCH_ASSOC ) )
				{
					echo
					"
						<option value='".$row['id_categoria']."'> ".$row['categoria_nome']." </option>
					";
				}
				
			} else {
				echo "<option value='0'> Não há Categorias disponíveis ainda </option>"; 
			}
			
			
			echo "</select>";
		}
		
		public function ListaCategoriaHome ( $pdo )
		{
			$sql = $this -> sqlListaCategoria( false, false, true);
			$res = $pdo -> query ($sql);
			
			if ( $res -> rowCount() > 0 )
			{
				echo 
				"
					<ul class='nav nav-pills nav-stacked'>
				";
				
				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					echo 
					"
						<li>
							<a href='?mod=CategoriaSub&opt=Lista-Categoria-Sub&id_categoria={$row['id_categoria']}'> 
								{$row['categoria_nome']} 
							</a>
						</li>
					";
						
				}
				echo "</ul>";
				
			} else {
				echo "<div class='div-fail'> <p> Não foram cadastradas categorias ainda. </p> </div> ";
			}
		}
		
		public function updateCategoria ( $pdo, $id_categoria, $categoria )
		{
			$sql =
			"
				UPDATE 
					categoria
				SET
					`categoria_nome` = '$categoria'
				WHERE
					`id_categoria` = '$id_categoria'
			";
			
			$res = $pdo -> query ($sql);
			
			if ( $res != null)
				echo "<div class='div-sucess'> <p> A categoria foi atualizada com sucesso, já disponível para visualização </p> </div>";
			else
				echo "<div class='div-fail'> <p> Houve um erro ao atualizar essa categoria. Envie a linha a seguir ao desenvolvedor: <hr> $sql </p> </div>";
			
		}
		
		public function incluirCategoria($pdo, $categoria_nome)
		{
			$sql =
			"
				INSERT INTO
					categoria
					(`categoria_nome`)
					values
					('$categoria_nome')
			";
			
			$res = $pdo -> query ($sql);
			
			if ( $res != null)
				echo "<div class='div-sucess'> <p> Categoria adicionada com sucesso, já disponível para visualização </p> </div>";
			else
				echo "<div class='div-fail'> <p> Houve um erro ao criar essa categoria. Envie a linha a seguir ao desenvolvedor: <hr> $sql </p> </div>";
		}
		
	
	}
	
	$classe_categoria = new Categoria();

?>