<?php

	/*
		classe categoria_sub
	*/
	
	class CategoriaSub
	{
		public function sqlQuantidadeArtigosCategoria ( $id_categoria_sub )
		{
			$sql = 
			"
				SELECT 
					count(id_categoria_sub_arquivo) as qtde_artigos
				FROM
					categoria_sub_arquivo
				WHERE
					categoria_sub_id = '$id_categoria_sub'
			";
			
			return $sql;
		}
		
		public function sqlListaCategoriaSub ( $id_categoria_sub, $id_categoria, $categoria_sub_nome, $categoria_ativo )
		{
			$sql =
			"
				SELECT 
					cs.*,
					c.*
				FROM 
					categoria_sub cs
				INNER JOIN
					categoria c ON c.id_categoria = cs.categoria_id
				WHERE
					c.categoria_status = '1'
					
			";
			
			if ( $id_categoria_sub !== FALSE )
				$sql .= " AND cs.id_categoria_sub = '$id_categoria_sub' ";
			
			if ( $id_categoria !== FALSE )
				$sql .= " AND c.id_categoria = '$id_categoria' ";
			
			if ( $categoria_sub_nome !== FALSE )
				$sql .= " AND cs.categoria_sub_nome like '%$categoria_sub_nome%' ";
			
			if ( $categoria_ativo === TRUE )
				$sql .= " AND cs.categoria_sub_status = '1' ";
			
			$sql .= " ORDER BY c.categoria_nome desc, cs.categoria_sub_nome asc ";
			
			return $sql;
		}
		
		public function returnQuantidadeArquivosCategoria ( $pdo, $id_categoria_sub )
		{
			$sql = $this -> sqlQuantidadeArtigosCategoria($id_categoria_sub);
			$res = $pdo -> query ($sql);
			
			$dados_qtd = array();
			
			if ( $res -> rowCount() > 0 )
			{
				array_push ($dados_qtd, $sql );
				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					array_push($dados_qtd, $row);
				}
			}
			
			return $dados_qtd;
		}
		
		public function returnCategoriaSub ( $pdo, $limit, $id_categoria_sub, $id_categoria, $categoria_sub_nome, $categoria_sub_status )
		{
			$sql = $this -> sqlListaCategoriaSub ( $id_categoria_sub, $id_categoria, $categoria_sub_nome, $categoria_sub_status );
			$sql_s_l = $sql;
			$sql .= calculaLimit($limit);
			$res = $pdo -> query ($sql);
			
			//echo $sql;
			
			$dados_Categoria = array();
			
			if ( $res -> rowCount() > 0 )
			{
				array_push ($dados_Categoria, $sql_s_l );
				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					array_push($dados_Categoria, $row);
				}
			}
			
			return $dados_Categoria;
		}
		
		public function selectCategoriaSub ( $pdo, $id_categoria_sub )
		{
			$sql = $this -> sqlListaCategoriaSub ( $id_categoria_sub, false, false, true );
			//echo "$sql<hr>";
			$res = $pdo -> query ($sql);
			
			echo "<select name='id_categoria_sub' > ";
			
			$publicacao = "";
			
			if ( $res -> rowCount() > 0 )
			{
				while  ($row = $res -> fetch ( PDO::FETCH_ASSOC ) )
				{
					
					if ( $publicacao != $row['categoria_nome'] )
					{	
						$publicacao = $row['categoria_nome'];
						echo
						"
							<optgroup label='$publicacao'> </optgroup>
						";
					}	
					
					
					echo
					"
						<option value='".$row['id_categoria_sub']."'> ".$row['categoria_sub_nome']." </option>
					";
				}
				
			} else {
				echo "<option value='0'> Não há Publicações disponíveis ainda </option>"; 
			}
			
			
			echo "</select>";
		}
		
		public function updateCategoriaSub ( $pdo, $id_categoria_sub, $id_categoria, $categoria_sub_nome )
		{
			$sql =
			"
				UPDATE 
					categoria_sub
				SET
					`categoria_sub_nome` = '$categoria_sub_nome',
					`categoria_id` = '$id_categoria'
				WHERE
					`id_categoria_sub` = '$id_categoria_sub'
			";
			
			echo $sql;
			
			$res = $pdo -> query ($sql);
			
			if ( $res != null)
				echo "<div class='div-sucess'> <p> A sub categoria foi atualizada com sucesso, já disponível para visualização </p> </div>";
			else
				echo "<div class='div-fail'> <p> Houve um erro ao atualizar essa sub categoria. Envie a linha a seguir ao desenvolvedor: <hr> $sql </p> </div>";
			
		}
		
		public function incluirCategoriaSub ($pdo, $categoria_id, $categoria_sub_nome)
		{
			$sql =
			"
				INSERT INTO
					categoria_sub
					(`categoria_id`,`categoria_sub_nome`)
					values
					('$categoria_id','$categoria_sub_nome')
			";
			
			$res = $pdo -> query ($sql);
			
			if ( $res != null)
				echo "<div class='div-sucess'> <p> Sub Categoria adicionada com sucesso, já disponível para visualização </p> </div>";
			else
				echo "<div class='div-fail'> <p> Houve um erro ao criar essa sub categoria. Envie a linha a seguir ao desenvolvedor: <hr> $sql </p> </div>";
		}
		
	
	}
	
	$classe_categoria_sub = new CategoriaSub();

?>