<?php

	/*
		classe arquivo
	*/
	
	class Arquivo
	{
		
		public function sqlListaArquivo ( $id_arquivo, $id_categoria_sub, $id_categoria )
		{
			$sql =
			"
				SELECT 
					cs.*,
					c.*,
					a.*
				FROM
					categoria_sub_arquivo a 
				INNER JOIN  
					categoria_sub cs ON cs.id_categoria_sub = a.categoria_sub_id 
				INNER JOIN
					categoria c ON c.id_categoria = cs.categoria_id
				WHERE
					c.categoria_status = '1'
					and a.arquivo_status = '1'
					
			";
			
			if ( $id_arquivo !== FALSE )
				$sql .= " AND a.id_categoria_sub_arquivo = '$id_arquivo' ";
			
			if ( $id_categoria_sub !== FALSE )
				$sql .= " AND cs.id_categoria_sub = '$id_categoria_sub' ";
			
			if ( $id_categoria !== FALSE )
				$sql .= " AND c.id_categoria = '$id_categoria' ";
			
			return $sql;
		}
		
		public function UltimosArquivosHome ($pdo)
		{
			$sql = $this ->  sqlListaArquivo ( FALSE, FALSE, FALSE );
			$sql .= " ORDER by a.id_categoria_sub_arquivo DESC LIMIT 10 ";
			$res = $pdo -> query ($sql);
			
			if ( $res -> rowCount() > 0 )
			{
				echo 
				"
					<div class='list-group'>
						<div class='list-group-item active'>
							Últimos artigos adicionados
						</div>
				";

				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					echo 
					"
						<a href='?mod=Arquivo&opt=Arquivo-Detalhe&id_arquivo={$row['id_categoria_sub_arquivo']}' class='list-group-item'>
							{$row['arquivo_nome']}
						</a>
					";
				}
				echo "</div>";
			} else {
				echo "<div class='div-fail'> <p> Nenhum arquivo enviado </p> </div>";
			}
		}
		
		public function returnArquivo ( $pdo, $limit, $id_arquivo,  $id_categoria_sub, $id_categoria )
		{
			$sql = $this -> sqlListaArquivo ( $id_arquivo, $id_categoria_sub, $id_categoria );
			$sql .= " ORDER BY a.arquivo_nome ASC ";
			$sql_s_l = $sql;
			$sql .= calculaLimit($limit);
			$res = $pdo -> query ($sql);
			
			//echo $sql;
			
			$dados_Categoria = array();
			
			if ( $res -> rowCount() > 0 )
			{
				array_push ($dados_Categoria, $sql_s_l );
				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					array_push($dados_Categoria, $row);
				}
			}
			
			return $dados_Categoria;
		}
		
		public function returnPesquisaArquivo ( $pdo, $dado_pesquisa )
		{
			$sql = $this -> sqlListaArquivo ( FALSE, FALSE, FALSE );
			$sql .= 
			" 
				AND  
					( 
						a.arquivo_nome like '%$dado_pesquisa%'
						or a.arquivo_autores like '%$dado_pesquisa%'
						or a.arquivo_resumo like '%$dado_pesquisa%'
						or a.arquivo_abstract like '%$dado_pesquisa%'
						or a.arquivo_keyword like '%$dado_pesquisa%'
						or cs.categoria_sub_nome like '%$dado_pesquisa%'
						or c.categoria_nome like '%$dado_pesquisa%'
					)
			";
			
			$res = $pdo -> query ($sql);
			
			if ( $res -> rowCount() > 0 )
			{
				
				echo "<p> Resultados encontrados: {$res -> rowCount()} </p> ";
				
				while ( $row = $res -> fetch (PDO::FETCH_ASSOC) )
				{
					//var_dump($row);
					echo 
					"
						<a href='?mod=Arquivo&opt=Arquivo-Detalhe&id_arquivo={$row['id_categoria_sub_arquivo']}'>
							<div class='panel panel-default'>
								<div class='panel-heading'> 
									{$row['arquivo_nome']}
								</div>
								
								<div class='panel-body'>
									<p> Categoria: {$row['categoria_nome']} </p>
									<p> Sub-Categoria: {$row['categoria_sub_nome']} </p>
								</div>
							</div>
						</a>
					";
				}
				
			} else {
				echo "<p> Não há registros com esses dados, tente trocar a palavra </p>";
			}
		}
		
		
		public function updateArquivo ( $pdo, $id_arquivo, $arquivo_titulo, $arquivo, $id_categoria_sub, $arquivo_abstract, $arquivo_resumo, $arquivo_autor )
		{
			$sql =
			"
				UPDATE 
					categoria_sub_arquivo
				SET
					`categoria_sub_id` = '$id_categoria_sub',
					`arquivo_nome` = '$arquivo_titulo',
					`arquivo_autores` = '$arquivo_autor',
					`arquivo_resumo` = '$arquivo_resumo',
					`arquivo_abstract` = '$arquivo_abstract'
				WHERE
					`id_categoria_sub_arquivo` = '$id_arquivo'
			";
			
			//echo $sql;
			
			$res = $pdo -> query ($sql);
			
			if ( $res != null)
				echo "<div class='div-sucess'> <p> A sub categoria foi atualizada com sucesso, já disponível para visualização </p> </div>";
			else
				echo "<div class='div-fail'> <p> Houve um erro ao atualizar essa sub categoria. Envie a linha a seguir ao desenvolvedor: <hr> $sql </p> </div>";
			
		}
		
		public function incluirArquivo ($pdo, $arquivo_titulo, $arquivo, $id_categoria_sub, $arquivo_abstract, $arquivo_resumo, $arquivo_autor )
		{
			/*
				Confere primeiro se o arquivo é válido
			*/
			//var_dump ($obra_arquivo);
			
			if ( $arquivo['type'] == "application/pdf")
			{
				if ( $arquivo['error'] == 0 )
				{
					
					
					$sql = 
					"	
						INSERT INTO
							categoria_sub_arquivo
							(`categoria_sub_id`,`arquivo_nome`,`arquivo_caminho`,`arquivo_autores`,`arquivo_resumo`,`arquivo_abstract`,`arquivo_quantidade_paginas`)
							VALUES
							('$id_categoria_sub','$arquivo_titulo','arc','$arquivo_autor','$arquivo_resumo','$arquivo_abstract','1')
					";
					
					$res = $pdo -> query ($sql);
					
					if ( $res != null)
					{	
						echo "<div class='div-sucess'> <p> Artigo adicionado com sucesso, já disponível para visualização </p> </div>";
						$target_dir = "upload/";
						$target_file = $target_dir . $pdo -> lastInsertId()."_".date("U").".pdf";
						move_uploaded_file($arquivo["tmp_name"], $target_file );
						
						$quantidade_paginas = paginasArquivo ($target_file);
						
						$sql_u = " UPDATE categoria_sub_arquivo SET arquivo_caminho = '$target_file', arquivo_quantidade_paginas = '$quantidade_paginas' WHERE id_categoria_sub_arquivo = '".$pdo -> lastInsertId()."' ";
						$res = $pdo -> query ($sql_u);
					}
					else
						echo "<div class='div-fail'> <p> Houve um erro ao criar esse Artigo. Envie a linha a seguir ao desenvolvedor: <hr> $sql </p> </div>";
				} else {
					echo "<div class='div-fail'> <p> O arquivo tem algum erro, tente novamente </p> </div>";
				}
			} else {
				echo "<div class='div-fail'> <p> O arquivo não tem um formato válido. O arquivo deve ser PDF </p> </div>";
			}
		}
		
	
	}
	
	$classe_arquivo = new Arquivo();

?>